//
//  UIColor+Utils.m
//  ColorShot
//
//  Created by Diogo Nunes on 06/09/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+ (UIColor *)averageColorFromArray:(NSArray *)colors {
    
    CGFloat averageRed = 0.0f;
    CGFloat averageGreen = 0.0f;
    CGFloat averageBlue = 0.0f;
    CGFloat averageAlpha = 0.0f;
    
    CGFloat blue = 0.0f;
    for (UIColor *color in colors) {
        CGColorRef colorRef = [color CGColor];
        
        size_t numComponents = CGColorGetNumberOfComponents(colorRef);
        const CGFloat *components = CGColorGetComponents(colorRef);
        CGFloat colorRed = 0.0f;
        CGFloat colorGreen = 0.0f;
        CGFloat colorBlue = 0.0f;
        CGFloat colorAlpha = 0.0f;
        
        if (numComponents >= 3)
        {
            colorRed = components[0];
            colorGreen = components[1];
            colorBlue = components[2];
        }
        if (numComponents >= 4) {
            colorAlpha = components[3];
        }
        averageRed += colorRed;
        averageGreen += colorGreen;
        averageBlue += colorBlue;
        averageAlpha +=colorAlpha;
    }
    
    return [UIColor colorWithRed:averageRed green:averageGreen blue:averageBlue alpha:averageAlpha];
}

@end
