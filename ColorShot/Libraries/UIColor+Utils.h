//
//  UIColor+Utils.h
//  ColorShot
//
//  Created by Diogo Nunes on 06/09/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

+ (UIColor *)averageColorFromArray:(NSArray *)colors;

@end
