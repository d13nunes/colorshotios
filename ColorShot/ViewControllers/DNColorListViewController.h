//
//  DNColorListViewController.h
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHColorPickerView.h"

@interface DNColorListViewController : UIViewController</*UITableViewDataSource, UITableViewDelegate,*/ CHColorPickerViewDelegate>

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet CHColorPickerView *colorView;

@end
