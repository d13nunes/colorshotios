//
//  DNCameraViewController.h
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DNRingView;

@interface DNCameraViewController : UIViewController

@property (nonatomic) IBOutlet DNRingView *colorRing;
@property (nonatomic) IBOutlet UIImageView *debugImageView;
@property (nonatomic) IBOutlet UIView *cameraView;
@end
