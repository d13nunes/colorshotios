//
//  DNTabBarController.m
//  ColorShot
//
//  Created by Xpand IT on 29/07/14.
//  Copyright (c) 2014 Diogo Nunes. All rights reserved.
//

#import "DNTabBarController.h"
#import "UIColor+AppColors.h"

@interface DNTabBarController ()

@end

@implementation DNTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.barTintColor = [UIColor barTintColor];
    [[UITabBar appearance] setTintColor:[UIColor barTitleColor]];

    UITabBarItem *firstItem = self.tabBar.items[0];
    UIImage * photo = [UIImage imageNamed:@"photo.png"];
    [firstItem setImage:photo];
    [firstItem setSelectedImage:photo];
    
    UITabBarItem *secondItem = self.tabBar.items[1];
    UIImage *camera = [UIImage imageNamed:@"camera.png"];
    [secondItem setImage:camera];
    [secondItem setSelectedImage:camera];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
