//
//  DNCameraViewController.m
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "DNCameraViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>
#import "DNColors.h"
#import "ColorUtils.h"
#import "DNRingView.h"
#import "UIImage+ColorPicker.h"
#import "UIColor+Utils.h"

@interface DNCameraViewController ()<AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) UIColor *lastColor;
@property (nonatomic) CGFloat lastScale;
@property (nonatomic) UIImage *lastImage;

@end

@implementation DNCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"#ColorShot";
    [self setupCamera];
    self.canDisplayBannerAds = YES;
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.session startRunning];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.session stopRunning];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupCamera {
    
    self.session = [[AVCaptureSession alloc] init];
    [self.session setSessionPreset:AVCaptureSessionPresetHigh];
    AVCaptureDevice *inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:&error];
    AVCaptureDevice *device = [deviceInput device];
    [device lockForConfiguration:nil];
    device.focusMode = AVCaptureFocusModeLocked;
    [device unlockForConfiguration];
    
    if ([self.session canAddInput:deviceInput]) {
        [self.session addInput:deviceInput];
    }
    
    AVCaptureVideoDataOutput *videoDataOutput = [AVCaptureVideoDataOutput new];
    NSDictionary *newSettings = @{ (NSString *)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA) };
    videoDataOutput.videoSettings = newSettings;
    [videoDataOutput setAlwaysDiscardsLateVideoFrames:YES];
    
    dispatch_queue_t videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
    [videoDataOutput setSampleBufferDelegate:self queue:videoDataOutputQueue];
    
    if ([self.session canAddOutput:videoDataOutput]) {
        [self.session addOutput:videoDataOutput];
    }
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    CALayer *rootLayer = self.view.layer;
    [rootLayer setMasksToBounds:YES];
    [previewLayer setFrame:CGRectMake(0, 0, rootLayer.bounds.size.width, rootLayer.bounds.size.height)];
    [rootLayer insertSublayer:previewLayer atIndex:0];
}

- (IBAction)addColor:(id)sender {
    
    self.debugImageView.image = self.lastImage;
    return;
    DNColor *color = [[DNColor alloc] init];
    color.color = self.lastColor;
    NSString *message = [NSString stringWithFormat:@"Color %@ was added", [color stringRGB]];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"New Color"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    DNColors *sharedColors = [DNColors sharedColors];
    [sharedColors.colors addObject:color];
    [sharedColors saveInCache];
}

#pragma mark - Delegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    [self processPixelBuffer:imageBuffer];
    // Create a UIImage from the sample buffer data
//    dispatch_sync(dispatch_get_main_queue(), ^{
//        UIImage *photoImage = [self imageFromSampleBuffer:sampleBuffer];
//        CGRect centerRect = CGRectMake(0, (int)(photoImage.size.height / 2), photoImage.size.width, (int)(photoImage.size.height / 2));
//        CGImageRef imageRef = CGImageCreateWithImageInRect([photoImage CGImage], centerRect);
//        // or use the UIImage wherever you like
//        self.lastImage = [UIImage imageWithCGImage:imageRef];
//        CGImageRelease(imageRef);
//        
//        
//        NSLog(@"phto %@", NSStringFromCGSize(photoImage.size));
//        NSLog(@"phto  %@", NSStringFromCGSize(photoImage.size));
//        //        NSLog(@"center %@", NSStringFromCGPoint(centerImage));
//        //        NSArray *colors = [photoImage RGBAsAtX:centerImage.x andY:centerImage.y count:10];
//        //        NSLog(@"%@", colors);
//        //        self.lastColor = [photoImage getAverageColorAtLocation:centerImage withRadius:10];
//        [self continuosRingUpdateWithLastColor];
//    });
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
}



- (UIColor *)averageColorAtRect:(CGRect)rect {
    UIImage *image = [UIImage alloc] ;
    NSInteger pixelCount = rect.size.width * rect.size.height;
    NSInteger c = 0;
    NSInteger r = 0;
    
    CGFloat red = 0.0f;
    CGFloat green = 0.0f;
    CGFloat blue = 0.0f;
    
    for (NSInteger i = 0; i < pixelCount; ++i) {
        CGPoint pixelPoint = CGPointMake(r, c);
        UIColor *pixelColor = [image colorAtPosition:pixelPoint];
        red += pixelColor.red;
        green += pixelColor.green;
        blue += pixelColor.blue;
    }
    return [UIColor alloc];
}

// Create a UIImage from sample buffer data
- (UIImage *)imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    /*Lock the image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    /*Get information about the image*/
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    /*Create a CGImageRef from the CVImageBufferRef*/
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef newImage = CGBitmapContextCreateImage(newContext);
    
    /*We release some components*/
    CGContextRelease(newContext);
    CGColorSpaceRelease(colorSpace);
    
    /*We display the result on the image view (We need to change the orientation of the image so that the video is displayed correctly).
     Same thing as for the CALayer we are not in the main thread so ...*/
    UIImage *image= [UIImage imageWithCGImage:newImage scale:1.0 orientation:UIImageOrientationRight];
    
    /*We relase the CGImageRef*/
    CGImageRelease(newImage);
    
    /*We unlock the  image buffer*/
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    return image;
}

#pragma mark - Debug

- (void)continuosRingUpdateWithLastColor {
    [self.colorRing setColor:self.lastColor];
}

#pragma mark - Pintch

- (IBAction)scale:(id)sender {
    
    [self.view bringSubviewToFront:[(UIPinchGestureRecognizer*)sender view]];
    if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        self.lastScale = 1.0;
    } else {
        UIView *transformView = self.colorRing;
        CGFloat currentScale = [[transformView.layer valueForKeyPath:@"transform.scale"] floatValue];
        
        CGFloat pinchscale = [(UIPinchGestureRecognizer*)sender scale];
        CGFloat scale = 1.0 - (self.lastScale - pinchscale);
        
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 0.5;
        
        scale = MIN(scale, kMaxScale / currentScale);
        scale = MAX(scale, kMinScale / currentScale);
        CGAffineTransform currentTransform = transformView.transform;
        CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
        [transformView setTransform:newTransform];
        self.lastScale = [(UIPinchGestureRecognizer*)sender scale];
    }
}

- (void)processPixelBuffer: (CVImageBufferRef)pixelBuffer {
    int BYTES_PER_PIXEL = 4;
    
    size_t bufferWidth = CVPixelBufferGetWidth(pixelBuffer);
    size_t bufferHeight = CVPixelBufferGetHeight(pixelBuffer);
    unsigned char *pixels = (unsigned char *)CVPixelBufferGetBaseAddress(pixelBuffer);
    
    for (int i = 0; i < (bufferWidth * bufferHeight); i++) {
        // Calculate the combined grayscale weight of the RGB channels
        int weight = (pixels[0] * 0.11) + (pixels[1] * 0.59) + (pixels[2] * 0.3);
        
        // Apply the grayscale weight to each of the colorchannels
        pixels[0] = weight; // Blue
        pixels[1] = weight; // Green
        pixels[2] = weight; // Red
        pixels += BYTES_PER_PIXEL;
    }
}

@end
