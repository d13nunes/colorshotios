//
//  DNColorListViewController.m
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <Social/Social.h>
#import <iAd/iAd.h>
#import "DNColorListViewController.h"
#import "DNCameraViewController.h"
#import "DNColors.h"
#import "NSString+SocialServices.h"
//#import "TableViewCell.h"

@interface DNColorListViewController ()//<SWRevealTableViewCellDataSource>

@end

@implementation DNColorListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.tableView.delegate = self;
    self.navigationItem.title = @"#ColorShot";
    
    self.colorView.delegate = self;
    self.colorView.colorsPerRow = 2;
    self.colorView.colorCellPadding = 1.0;
    
    self.colorView.highlightSelection = YES;
    self.colorView.selectionBorderColor = [UIColor whiteColor];
    
    self.colorView.backgroundColor = [UIColor blackColor];
    [self.colorView init];
    
//    UIBarButtonItem *switchViewButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(switchView)];
//    self.navigationItem.rightBarButtonItems = @[switchViewButton];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadData];
    self.canDisplayBannerAds = YES;
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.canDisplayBannerAds = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)reloadData {
    [self.tableView reloadData];
    [self.colorView setColors:[[DNColors sharedColors] arrayUIColors]];
}

//#pragma mark - UITableView Data Source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSInteger count = [DNColors sharedColors].colors.count;
//    return count;
//}
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return YES;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    DNColors *colors = [DNColors sharedColors];
//    DNColor *color = colors.colors[indexPath.row];
//    TableViewCell *cell = [TableViewCell cellForTableView:tableView];
//    [cell fillColor:color];
//    cell.dataSource = self;
//    return cell;
//}

#pragma mark - CHColorPickerViewDelegate

- (void)colorPickerView:(CHColorPickerView *)colorPickerView didSelectColorAtIndexPath:(NSIndexPath *)indexPath {
    [self showOptionsAtIndexPath:indexPath];
    
}


#pragma mark - rightButtonItemsInRevealTableViewCell

//- (NSArray*)rightButtonItemsInRevealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell {
//    SWCellButtonItem *item1 = [SWCellButtonItem itemWithTitle:@"Delete" handler:^(SWCellButtonItem *item, SWRevealTableViewCell *cell) {
//        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
//        [self deleteColorAtRow:indexPath.row];
//        [self.tableView beginUpdates];
//        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
//        [self.tableView endUpdates];
//        return NO;
//    }];
//    item1.backgroundColor = [UIColor redColor];
//    item1.tintColor = [UIColor whiteColor];
//    item1.width = 88;
//    
//    SWCellButtonItem *item2 = [SWCellButtonItem itemWithTitle:@"Share" handler:^(SWCellButtonItem *item, SWRevealTableViewCell *cell) {
//        NSString *color = [((TableViewCell *)cell).color stringRGB];
//        [self showSharingOptionsForString:color];
//        return YES;
//    }];
//    item2.backgroundColor = [UIColor blueColor];
//    item2.width = 88;
//    item2.tintColor = [UIColor whiteColor];
//    
//    SWCellButtonItem *item3 = [SWCellButtonItem itemWithTitle:@"" handler:^(SWCellButtonItem *item, SWRevealTableViewCell *cell) {
//        return NO;
//    }];
//    return @[item3, item2, item1];
//}

- (void)deleteColorAtRow:(NSInteger)row {
    DNColors *sharedColors = [DNColors sharedColors];
    [sharedColors.colors removeObjectAtIndex:row];
    [sharedColors saveInCache];
}

- (void)showSharingOptionsForString:(NSString *)color {
    NSMutableArray *systemServices = [NSMutableArray arrayWithObjects:
                                      SLServiceTypeFacebook,
                                      SLServiceTypeTwitter,
                                      SLServiceTypeSinaWeibo,
                                      SLServiceTypeTencentWeibo, nil];
    NSMutableArray *availableServices = [NSMutableArray array];
    for (NSString *service in systemServices) {
        if ([SLComposeViewController isAvailableForServiceType:service]) {
            [availableServices addObject:service];
        }
    }
    if (availableServices.count > 0) {
        UIAlertController *serviceChooser = [UIAlertController alertControllerWithTitle:@"Share"
                                                                                message:nil
                                                                         preferredStyle:UIAlertControllerStyleActionSheet];
        for (NSString *service in availableServices) {
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:[service stringHumanReadable] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                SLComposeViewController *composeVC = [SLComposeViewController composeViewControllerForServiceType:service];
                [composeVC setInitialText:[NSString stringWithFormat:@"Just found that RGB code %@ was I was looking for using #ColorShot :)", color]];
                [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
                [self presentViewController:composeVC animated:YES completion:nil];
            }];
            [serviceChooser addAction:alertAction];
        }
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel   handler:^(UIAlertAction *action) {
            [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
        }];
        [serviceChooser addAction:cancelAction];
        
        [self presentViewController:serviceChooser animated:YES completion:nil];
    } else {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Share"
                                                                           message:@"No sharing services available"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertView addAction:alertAction];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}

- (void)showOptionsAtIndexPath:(NSIndexPath *)indexPath {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Options" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *shareAction = [UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        DNColors *sharedColors = [DNColors sharedColors];
        DNColor *color = sharedColors.colors[indexPath.row];
        NSString *colorStr = [color stringRGB];
        [self showSharingOptionsForString:colorStr];
    } ];
    [alertController addAction:shareAction];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self deleteColorAtRow:indexPath.row];
        UICollectionView *collectionView = self.colorView.colorView;
        [collectionView performBatchUpdates:^{
            [self.colorView setColors:[[DNColors sharedColors] arrayUIColors]];
            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:nil];
    }];
    [alertController addAction:deleteAction];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)switchView {
    UIView *view1 = self.tableView.hidden ? self.colorView : self.tableView;
    UIView *view2 = self.tableView.hidden ? self.tableView : self.colorView;
    view1.hidden = NO;
    view2.hidden = NO;
    [UIView transitionFromView:view1
                        toView:view2
                      duration:0.75
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished) {
                        view1.hidden = YES;
                    }];
}

@end
