//
//  DNNavigationController.m
//  ColorShot
//
//  Created by Xpand IT on 24/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "DNNavigationController.h"
#import "DNNavigationBar.h"
#import "UIColor+AppColors.h"

@implementation DNNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.barTintColor = [UIColor barTintColor];
    
    UIFont *font = [UIFont boldSystemFontOfSize:24];
//    NSShadow *shadow = [[NSShadow alloc] init];
//    shadow.shadowColor = [UIColor whiteColor];
//    shadow.shadowOffset = CGSizeMake(0, -1);
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor barTitleColor],
//                                               NSShadowAttributeName : shadow,
                                               NSFontAttributeName : font};
}

@end
