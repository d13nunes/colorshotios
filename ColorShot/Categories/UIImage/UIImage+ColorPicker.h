//
//  UIColor+ColorPicker.h
//  ColorShot
//
//  Created by Xpand IT on 24/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImage (ColorPicker)

- (UIColor *)colorAtPosition:(CGPoint)position;
- (NSArray*)RGBAsAtX:(int)xx andY:(int)yy count:(int)count;
- (UIColor *)getAverageColorAtLocation:(CGPoint)point withRadius:(int)radialSize;
@end
