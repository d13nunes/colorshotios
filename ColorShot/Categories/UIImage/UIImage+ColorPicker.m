//
//  UIColor+ColorPicker.m
//  ColorShot
//
//  Created by Xpand IT on 24/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "UIImage+ColorPicker.h"

@implementation UIImage (ColorPicker)

- (UIColor *)colorAtPosition:(CGPoint)position {
    CGRect sourceRect = CGRectMake(position.x, position.y, 1.f, 1.f);
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, sourceRect);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *buffer = malloc(4);
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big;
    CGContextRef context = CGBitmapContextCreate(buffer, 1, 1, 8, 4, colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0.f, 0.f, 1.f, 1.f), imageRef);
    CGImageRelease(imageRef);
    CGContextRelease(context);
    
    CGFloat r = buffer[0] / 255.f;
    CGFloat g = buffer[1] / 255.f;
    CGFloat b = buffer[2] / 255.f;
    CGFloat a = buffer[3] / 255.f;
    
    free(buffer);
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

- (UIColor *) GetCurrentPixelColorAtPoint:(CGPoint)point inView:(UIView *)view
{
    unsigned char pixel[4] = {0};
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -point.x, -point.y);
    
    [view.layer renderInContext:context];
    
    CGContextRelease(context);
    
    CGColorSpaceRelease(colorSpace);
    
    NSLog(@"pixel: %d %d %d %d", pixel[0], pixel[1], pixel[2], pixel[3]);
    
    UIColor *color = [UIColor colorWithRed:pixel[0]/255.0 green:pixel[1]/255.0 blue:pixel[2]/255.0 alpha:pixel[3]/255.0];
    
    return color;
}

- (NSArray*)RGBAsAtX:(int)xx andY:(int)yy count:(int)count
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];
    
    // First get the image into your data buffer
    CGImageRef imageRef = [self CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    long byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
    for (int ii = 0 ; ii < count ; ++ii)
    {
        CGFloat red   = (rawData[byteIndex]     * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue  = (rawData[byteIndex + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        byteIndex += 4;
        
        UIColor *acolor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        [result addObject:acolor];
    }
    
    free(rawData);
    
    return result;
}


- (UIColor *)getAverageColorAtLocation:(CGPoint)point withRadius:(int)radialSize
{
    int sampleSize = (radialSize * 2) + 1;
    int pixelsToCount = sampleSize * sampleSize;
    
    int xStartPoint = point.x - radialSize;
    int yStartPoint = point.y - radialSize;
    
    // Make sure we are not running off the edge of the image
    if(xStartPoint < 0){
        xStartPoint = 0;
    }
    else if (xStartPoint + sampleSize > self.size.width){
        xStartPoint = self.size.width - sampleSize - 1;
    }
    
    if(yStartPoint < 0){
        yStartPoint = 0;
    }
    else if (yStartPoint + sampleSize > self.size.height){
        yStartPoint = self.size.height - sampleSize - 1;
    }
    
    // This is the representation of the pixels we would like to average
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, CGRectMake(xStartPoint, yStartPoint, sampleSize, sampleSize));
    
    // Get the image dimensions
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    
    // Set the color space to RGB
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Understand the bit length of the data
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = (NSUInteger)8;
    
    // Set up a Common Graphics Context
    CGContextRef context = CGBitmapContextCreate(rawData, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (int)((bytesPerRow * 0) + 0 * bytesPerPixel);
    
    CGFloat cumulativeRed = 0.0;
    CGFloat cumulativeGreen = 0.0;
    CGFloat cumulativeBlue = 0.0;
    
    for (int ii = 0 ; ii < pixelsToCount ; ++ii){
        CGFloat red   = (rawData[byteIndex]     * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue  = (rawData[byteIndex + 2] * 1.0) / 255.0;
        __unused CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        
        cumulativeRed += red;
        cumulativeGreen += green;
        cumulativeBlue += blue;
        
        byteIndex += 4;
    }
    
    CGFloat avgRed = (cumulativeRed / pixelsToCount);
    CGFloat avgGreen = (cumulativeGreen / pixelsToCount);
    CGFloat avgBlue = (cumulativeBlue / pixelsToCount);
    
    free(rawData);
    
    return [UIColor colorWithRed:avgRed green:avgGreen blue:avgBlue alpha:1.0];
}

@end
