//
//  NSString+SocialServices.m
//  ColorShot
//
//  Created by Xpand IT on 23/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "NSString+SocialServices.h"
#import <Social/Social.h>

@implementation NSString (SocialService)

- (NSString *)stringHumanReadable {
    static NSDictionary *__socialServicesHumanReadableStrings;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __socialServicesHumanReadableStrings = @{
                                                 SLServiceTypeFacebook      : @"Facebook",
                                                 SLServiceTypeTwitter       : @"Twitter",
                                                 SLServiceTypeSinaWeibo     : @"Sina Weibo",
                                                 SLServiceTypeTencentWeibo  : @"Tencent Weibo"
                                                 };
    });
    return __socialServicesHumanReadableStrings[self];
}

@end


