//
//  NSString+SocialServices.h
//  ColorShot
//
//  Created by Xpand IT on 23/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SocialServices)

- (NSString *)stringHumanReadable;

@end