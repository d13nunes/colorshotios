//
//  UIColor+AppColors.m
//  ColorShot
//
//  Created by Xpand IT on 29/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "UIColor+AppColors.h"

@implementation UIColor (AppColors)

+ (UIColor *)barTintColor {
    return [UIColor colorWithRed:0.0 green:249.0/255.0 blue:79.0/255.0 alpha:1.0];
}
+ (UIColor *)barTitleColor {
    return [UIColor colorWithRed:19.0/255.0 green:46.0/255.0 blue:200.0/255.0 alpha:1.0];//[UIColor colorWithRed:1 green:202.0/255.0 blue:70.0/255.0 alpha:1.0];
}

@end
