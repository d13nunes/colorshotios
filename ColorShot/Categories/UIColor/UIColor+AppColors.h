//
//  UIColor+AppColors.h
//  ColorShot
//
//  Created by Xpand IT on 29/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (AppColors)

+ (UIColor *)barTintColor;
+ (UIColor *)barTitleColor;

@end
