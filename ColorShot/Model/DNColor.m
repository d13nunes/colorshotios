//
//  DNColor.m
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "DNColor.h"
#import "ColorUtils.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"
#import "MTLValueTransformer.h"

@implementation DNColor

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"color" : @"color"};
}

+ (NSValueTransformer *)colorJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *colorStr) {
        return [UIColor colorWithString:colorStr];
    } reverseBlock:^(UIColor *color) {
        return [color stringValue];
    }];
}

- (NSString *)description {
    return [self.color stringValue];
}

- (NSString *)stringRGB {
    return [[self.color stringRGB] uppercaseString];
}

@end
