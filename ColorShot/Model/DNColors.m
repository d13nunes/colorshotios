//
//  DNColors.m
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "DNColors.h"
#import "NSValueTransformer+MTLPredefinedTransformerAdditions.h"

static NSString *cachePath = @"savedColor";


@implementation DNColors

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"colors" : @"colors"};
}

+ (NSValueTransformer *)colorsJSONTransformer {
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:DNColor.class];
}

+ (instancetype)sharedColors {
    static DNColors *__shared_colors;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shared_colors = [DNColors loadFromCache];
    });
    return __shared_colors;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.colors = [NSMutableArray array];
    }
    return self;
}

+ (instancetype)loadFromCache {
    NSError *error = nil;
    NSDictionary *dicLoaded = [NSDictionary dictionaryWithContentsOfFile:[[self class] filePath]];
    DNColors *colors = [MTLJSONAdapter modelOfClass:DNColors.class
                                 fromJSONDictionary:dicLoaded
                                              error:&error];
    if (!colors || error) {
        colors = [[DNColors alloc] init];
    }
    return colors;
}

- (void)saveInCache {
    // store in cache
    NSDictionary *dicToSave = [MTLJSONAdapter JSONDictionaryFromModel:self];
    BOOL success = [dicToSave writeToFile:[[self class] filePath] atomically:NO];
    NSLog(@"%@", success?@"YES" : @"NO");
}

- (NSArray *)arrayUIColors {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:self.colors.count];
    for (DNColor *color in self.colors) {
        if (color.color) {
            [array addObject:color.color];
        }

    }
    return array;
}

+ (NSString *)filePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:cachePath];
    return appFile;
}

@end
