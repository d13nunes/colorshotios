//
//  DNColor.h
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface DNColor : MTLModel<MTLJSONSerializing>

@property (nonatomic) UIColor *color;

- (NSString *)description;
- (NSString *)stringRGB;

@end
