//
//  DNColors.h
//  ColorShot
//
//  Created by Xpand IT on 21/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLJSONAdapter.h"
#import "DNColor.h"

@interface DNColors : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSMutableArray *colors;

+ (instancetype)sharedColors;

+ (instancetype)loadFromCache;
- (void)saveInCache;
- (NSArray *)arrayUIColors;

@end
