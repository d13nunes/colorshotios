//
//  DNNavigationBAr.m
//  ColorShot
//
//  Created by Xpand IT on 24/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "DNNavigationBar.h"

@implementation DNNavigationBar

- (UIColor *)barTintColor {
    return [UIColor colorWithRed:0 green:249 blue:80 alpha:1];
}

@end
