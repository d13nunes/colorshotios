//
//  TableViewCell.h
//  ColorShot
//
//  Created by Xpand IT on 22/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealTableViewCell.h"

@class DNColor;

@interface TableViewCell : SWRevealTableViewCell

@property (nonatomic) IBOutlet UILabel *label;
@property (nonatomic) IBOutlet UIView *colorView;

@property (nonatomic) DNColor *color;

+ (NSString *)identifier;
+ (UINib *)nib;
+ (NSString *)nibName;

+ (id)cellForTableView:(UITableView *)tableViewView fromNib:(UINib *)nib;
+ (id)cellForTableView:(UITableView *)tableViewView;

- (void)fillColor:(DNColor *)color;

@end
