//
//  DNColorCollectionViewCell.h
//  ColorShot
//
//  Created by Xpand IT on 03/08/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DNColorCollectionViewCell : UICollectionViewCell

@property (nonatomic) IBOutlet UILabel *colorLabel;

@end
