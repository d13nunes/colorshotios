//
//  TableViewCell.m
//  ColorShot
//
//  Created by Xpand IT on 22/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "TableViewCell.h"
#import "DNColor.h"

@implementation TableViewCell

+ (NSString *)identifier {
    return @"TableViewCell";
}

#pragma mak - Static Methods


+ (UINib *)nib
{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    return [UINib nibWithNibName:[self nibName] bundle:classBundle];
}

+ (NSString *)nibName
{
    return [self cellIdentifier];
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

+ (id)cellForTableView:(UITableView *)tableView
{
    return [self cellForTableView:tableView fromNib:[self nib]];
}

+ (id)cellForTableView:(UITableView *)tableView fromNib:(UINib *)nib
{
    NSString *cellID = [self cellIdentifier];
    SWRevealTableViewCell *cell = (SWRevealTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [self cellFromNib:nib];
        cell.bounceBackOnRightOverdraw = YES;
        cell.cellRevealMode = SWCellRevealModeNormalWithBounce;
    }
    return cell;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
}

+ (id)cellFromNib:(UINib *)nib
{
    NSArray *nibObjects = [nib instantiateWithOwner:nil options:nil];
    NSAssert2(([nibObjects count] > 0)  &&
              [[nibObjects objectAtIndex:0] isKindOfClass:[self class]],
              @"nibName ‘ %@’ does not appear to contain a value of %@",
              [self nibName],
              NSStringFromClass([self class]));
    return [nibObjects objectAtIndex:0];
}

- (NSString *)reuseIdentifier
{
    return [self.class cellIdentifier];
}

- (void)fillColor:(DNColor *)color {
    self.colorView.backgroundColor = color.color;
    self.label.text = [color stringRGB];
    self.color = color;
}

@end
