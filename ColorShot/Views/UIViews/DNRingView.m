//
//  DNColorRing.m
//  ColorShot
//
//  Created by Xpand IT on 22/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import "DNRingView.h"

@interface DNRingView ()

@property (nonatomic) CAShapeLayer *shapeLayer;

@end

@implementation DNRingView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setRing];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setRing];
    }
    return self;
}

- (void)setRing {
    CGRect frame = self.bounds;
    self.shapeLayer = [CAShapeLayer layer];
    static CGFloat lineWith = 10;
    self.shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(lineWith / 2, lineWith / 2, frame.size.width - lineWith, frame.size.width - lineWith)].CGPath;
    
    self.shapeLayer.frame = frame;
    self.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    self.shapeLayer.strokeColor = [UIColor orangeColor].CGColor;
    self.shapeLayer.lineWidth = lineWith;
    [self.layer addSublayer:self.shapeLayer];

    CAShapeLayer *innerRing = [CAShapeLayer layer];
    static CGFloat lineWithInnerRing = 2;
    innerRing.path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(lineWith,
                                                                       lineWith,
                                                                       frame.size.width - 2*lineWith,
                                                                       frame.size.width - 2*lineWith)].CGPath;
    innerRing.frame = frame;
    innerRing.fillColor = [UIColor clearColor].CGColor;
    innerRing.strokeColor = [UIColor whiteColor].CGColor;
    innerRing.lineWidth = lineWithInnerRing;
    [self.layer addSublayer:innerRing];
}

- (void)setColor:(UIColor *)color {
    self.shapeLayer.strokeColor = color.CGColor;
}

- (void)setFrame:(CGRect)frame animated:(BOOL)animated animationTime:(double)time{
   
}

@end
