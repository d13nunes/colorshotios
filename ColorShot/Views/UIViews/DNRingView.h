//
//  DNColorRing.h
//  ColorShot
//
//  Created by Xpand IT on 22/07/14.
//  Copyright (c) 2014 DiogoNunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DNRingView : UIView

- (void)setColor:(UIColor *)color;

@end
